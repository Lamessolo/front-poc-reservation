import { Component, OnInit } from '@angular/core';
import { Ireservation } from '../Ireservation';
import { ReservationService } from '../reservation.service';

@Component({
  selector: 'app-reservations',
  templateUrl: './reservations.component.html',
  styleUrls: ['./reservations.component.css']
})
export class ReservationsComponent implements OnInit {
 reservations!: Ireservation[];
 emetteur!:string;
 motCle!:string;
  constructor(private reservationService: ReservationService) { }

  ngOnInit(): void {
    this.reservationService.getReservation()
    .subscribe(reponse => {
         this.reservations = reponse;
    },error =>{console.log(error);}
    );
  }

 public SearchByEmetteur(){
  this.reservationService.getReservationsByEmmetteur(this.emetteur).subscribe(reponse => {
    this.reservations = reponse;
},error =>{console.log(error);}
); 
  }

  public SearchByMotCle(){
    this.reservationService.getReservationsByMotCle(this.motCle).subscribe(reponse => {
      this.reservations = reponse;
  },error =>{console.log(error);}
  ); 
}

public Clear(){
  this.reservationService.getReservation()
  .subscribe(reponse => {
       this.reservations = reponse;
  },error =>{console.log(error);}
  );
}
}
