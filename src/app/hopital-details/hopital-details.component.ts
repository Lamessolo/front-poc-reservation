import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HopitalService } from '../hopital.service';
import { Ihopital } from '../Ihopital';
import { ReservationService } from '../reservation.service';
import { specialisationInfo } from '../specialisationInfo';
import {MatDialogModule,MatDialogConfig,MatDialog} from '@angular/material/dialog';
import { ReservationFormComponent } from '../reservation-form/reservation-form.component';
import { FormBuilder, FormControl, FormGroup,FormsModule, NgForm } from '@angular/forms';
import { Ireservation } from '../Ireservation';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-hopital-details',
  templateUrl: './hopital-details.component.html',
  styleUrls: ['./hopital-details.component.css']
})
export class HopitalDetailsComponent implements OnInit {

public hopital:Ihopital= new Ihopital;
public reservation : Ireservation = new Ireservation();
hopitalRerservation : Ireservation[];
 specialisation!:specialisationInfo[];
public reservationForm: FormGroup;
  msgValidReservation: boolean=false;
  reservationFenetre: boolean  =false;

  hopitalName:string;
  constructor(private route:ActivatedRoute,private hopitalService:HopitalService,
  private reservationService:ReservationService, public dialog: MatDialog, private toastr: ToastrService) { }

  ngOnInit(): void {
    const gethopitalId = parseInt(this.route.snapshot.paramMap.get('id'));
   
  this.reservationForm = new FormGroup({
    date: new FormControl(''),
    emetteurName:new FormControl('')
    ,hopitalDestination:new FormControl(this.hopital.name)
    ,hopitalId:new FormControl(gethopitalId )
    ,nbrLitReservation: new FormControl('')
    ,status:new FormControl(''),
    details: new FormControl('')        
   });
  this.getHopitalByIdForDetail();
  this.getReservationByHopitalId();
}


onCreateReservation(){
 this.reservationFenetre=true;
}

onCloseReservation(){
  this.reservationFenetre=false;
}

public getHopitalByIdForDetail(){
  const gethopitalId = parseInt(this.route.snapshot.paramMap.get('id'));
  this.hopitalService.gethopitalById(gethopitalId).subscribe(reponse => {
    this.hopital = reponse;  
});

}

public getReservationByHopitalId(){
  const gethopitalId = parseInt(this.route.snapshot.paramMap.get('id'));
  this.reservationService.getReservationsByHopitalId(gethopitalId).subscribe(reponse => {
    this.hopitalRerservation = reponse;
 
});
}

onSubmitReservation(reservationForm:FormGroup){
  this.reservationService.createReservation(this.reservationForm.value).subscribe(data =>{
   this.msgValidReservation=true;
   this.getReservationByHopitalId();
   this.getHopitalByIdForDetail();
   this.onCloseReservation();
   this.toastr.success('Validé' + ":"+
   this.reservationForm.value.nbrLitReservation +' lit(s) en cours de reservation' , 'En cours');
  });
 
}
}


