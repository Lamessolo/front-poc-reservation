import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccidentsComponent } from './accidents/accidents.component';
import { HopitalDetailsComponent } from './hopital-details/hopital-details.component';
import { HopitalformComponent } from './hopitalform/hopitalform.component';
import { HopitalsComponent } from './hopitals/hopitals.component';
import { ReservationsComponent } from './reservations/reservations.component';
import { SpecialisationsComponent } from './specialisations/specialisations.component';

const routes: Routes = [
  { path: 'hopitals', component: HopitalsComponent},
  { path: 'specialisations', component: SpecialisationsComponent},
  { path: 'reservations', component: ReservationsComponent},
  { path: 'hopitals/:id', component: HopitalDetailsComponent},
  { path: 'hopital', component: HopitalformComponent},
  { path: 'accidents', component: AccidentsComponent}
 

];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [HopitalformComponent]