import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { specialisationInfo } from './specialisationInfo';

@Injectable({
  providedIn: 'root'
})
export class SpecialisationService {

 
  constructor(private httpClient: HttpClient) { }

  private baseUrlSpecialisation = environment.apiUrl +'specialisations';
  private baseUrlHopital = environment.apiUrl +'hopitals';  
  public  getspecialisation(): Observable<specialisationInfo[]>{  
    return this.httpClient.get<specialisationInfo[]>(this.baseUrlSpecialisation);
   }

   public getspecialisationByHopital(id:number): Observable<specialisationInfo[]>{

     return this.httpClient.get<specialisationInfo[]>(this.baseUrlHopital+'/'+id+'/specialisations');
   }
   public getspecialisationByNameOrSpecialite(search:string): Observable<specialisationInfo[]>{

    return this.httpClient.get<specialisationInfo[]>(this.baseUrlSpecialisation+'/search/'+search);
  }

}
