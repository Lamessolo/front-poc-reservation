import { Component,OnInit } from '@angular/core';
import { HopitalService } from './hopital.service';
import { Ihopital } from './Ihopital';
import { SpecialisationService } from './specialisation.service';
import { specialisationInfo } from './specialisationInfo';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'Reservation';
  public hopitals! : Ihopital[];
  public specialisations! : specialisationInfo[];
  constructor(private hopitalService: HopitalService,private specialisationService : SpecialisationService){}
  ngOnInit(): void {
  
  
  }

 
}
