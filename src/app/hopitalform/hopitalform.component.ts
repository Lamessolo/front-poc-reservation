import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup,FormBuilder } from '@angular/forms';
import { HopitalService } from '../hopital.service';
import { Ihopital } from '../Ihopital';
import { ReservationService } from '../reservation.service';
import { SpecialisationService } from '../specialisation.service';
import { specialisationInfo } from '../specialisationInfo';

@Component({
  selector: 'app-hopitalform',
  templateUrl: './hopitalform.component.html',
  styleUrls: ['./hopitalform.component.css']
})
export class HopitalformComponent implements OnInit {

 specialisations: specialisationInfo[]
 msgValidHopital=false;
 hopital : Ihopital = new Ihopital();
 hopitalForm: FormGroup
constructor(private specialisationService:SpecialisationService, private hopitalService:HopitalService,
  private fb : FormBuilder) { }

   
  ngOnInit(): void {
   this.getSpecialisation();
   this.hopitalForm = this.fb.group({
     id:'null',
    name:''
    ,litDispo:''
    ,ville:'',localite: ''
    ,status: 'Normal',litTotal:''
    ,litReanimation: '', specialisations:''
   });
  }

  public getSpecialisation(){
    this.specialisationService.getspecialisation().subscribe(reponse => {
      this.specialisations = reponse;
 },error =>{console.log(error);}
 );
  }

  public onSubmitHopital(){  
     
     console.log('valeurs:',JSON.stringify(this.hopitalForm.value));
 this.hopitalService.createHopital(this.hopitalForm.value).subscribe(data =>{ 
  this.msgValidHopital=true;
 });

}

}