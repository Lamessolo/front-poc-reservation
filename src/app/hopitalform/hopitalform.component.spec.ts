import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HopitalformComponent } from './hopitalform.component';

describe('HopitalformComponent', () => {
  let component: HopitalformComponent;
  let fixture: ComponentFixture<HopitalformComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HopitalformComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HopitalformComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
