import { Ihopital } from "./Ihopital";

export class Ireservation{
	id: number
	date: string
	emetteurName: string
	hopitalDestination?: string	
	nbrLitReservation: number
	details: string
	status: string
	hopitalId: number
}