import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import { accident } from './accident';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class AccidentService {

  constructor(private httpClient: HttpClient) { }

  private baseUrlAccidents = environment.apiUrl +'accidents';  
  public  getAccidents(): Observable<accident[]>{
      
     return this.httpClient.get<accident[]>(this.baseUrlAccidents);
    }

    public  getAccidentById(id:number): Observable<accident>{   
      return this.httpClient.get<accident[]>(this.baseUrlAccidents).pipe(
        map((hopitals:any[])=>hopitals.find(hopital=>hopital.id === id))
      );  
     }

     public createAccident(newAccident:accident){
      return this.httpClient.post(this.baseUrlAccidents+'/new',newAccident);
    }

}
