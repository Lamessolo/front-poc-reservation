import { specialisationInfo } from "./specialisationInfo";

export class Ihopital{
 // find(arg0: (hopital: any) => boolean): any;
 id: number;
 name: string;
 litDispo: number;
 ville: string;
 localite: string;
 status?: string;
 litTotal: number;
 litReanimation: number;
 specialisations: specialisationInfo[];



}