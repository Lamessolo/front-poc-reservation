import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Ireservation } from './Ireservation';

@Injectable({
  providedIn: 'root'
})
export class ReservationService {

  constructor(private httpClient: HttpClient) { }
  private baseUrlHopital = environment.apiUrl +'hopitals';  
  private baseUrlReservation = environment.apiUrl +'reservations';  

  public  getReservation(): Observable<Ireservation[]>{
    
    return this.httpClient.get<Ireservation[]>(this.baseUrlReservation);
   }

   public getReservationsByEmmetteur(emetteur:string): Observable<Ireservation[]>{
    return this.httpClient.get<Ireservation[]>(this.baseUrlReservation+'/emetteur/'+emetteur);
  }

  public getReservationsByMotCle(motCle:string): Observable<Ireservation[]>{
    return this.httpClient.get<Ireservation[]>(this.baseUrlReservation+'/motcle/'+motCle);
  }

  public getReservationsByHopitalId (id:number): Observable<Ireservation[]>{
    return this.httpClient.get<Ireservation[]>(this.baseUrlReservation+'/hopital/'+id);
  }


  public createReservation(createBody:FormGroup){
    return this.httpClient.post(this.baseUrlReservation+'/new',createBody);
  }
}
