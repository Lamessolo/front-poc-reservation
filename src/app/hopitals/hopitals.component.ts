import { Component, OnInit } from '@angular/core';
import { Ihopital } from '../Ihopital';
import { HopitalService } from '../hopital.service';
import { SpecialisationService } from '../specialisation.service';
import { specialisationInfo } from '../specialisationInfo';
import { isNull } from '@angular/compiler/src/output/output_ast';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MatDialog,MatDialogConfig } from '@angular/material/dialog';
import { HopitalformComponent } from '../hopitalform/hopitalform.component';

@Component({
  selector: 'app-hopitals',
  templateUrl: './hopitals.component.html',
  styleUrls: ['./hopitals.component.css']
})
export class HopitalsComponent implements OnInit {
  rForm:FormGroup;
  post:any;
public localite: string="";
public  specialisation:string="";
public specialisationId:number;
public hopitals!:Ihopital[];
//public newHopital :Ihopital=new Ihopital();
public  specialisations! : specialisationInfo[];
  specialisationsHopital! : specialisationInfo[];
  constructor(private hopitalService : HopitalService,
     private specialisationService : SpecialisationService) {
       
      }

  ngOnInit(): void {
   
   this.getHopital();
 this.getSpecialisation();
    }

    public getHopital(){
      this.hopitalService.gethopitals()
      .subscribe(reponse => {
           this.hopitals = reponse;
      },error =>{console.log(error);}
      );
    }

    public getSpecialisation(){
  this.specialisationService.getspecialisation().subscribe(reponse => {
    this.specialisations = reponse;
},error =>{console.log(error);}
);
}

    public SearchByLocalite(){     
      this.hopitalService.getHopitalByLocalite(this.localite).subscribe(reponse => {
          this.hopitals = reponse;
     },error =>{console.log(error);}
      );   
    }

    public SearchBySpecialisation(){
    this.hopitalService.getHopitalBySpecialisation(this.specialisation).subscribe(reponse => {
      this.hopitals = reponse;
 },error =>{console.log(error);}
  );
   }

    public SearchByLocaliteAndSpecialisation(localite:string,specialisation:string){
     if(localite ==""){ this.hopitalService.getHopitalBySpecialisation(this.specialisation)
      .subscribe(reponse => {
        this.hopitals = reponse;
   },error =>{console.log(error);}
    );
    }
    if(specialisation=="")
      { this.hopitalService.getHopitalByLocalite(this.localite)
        .subscribe(reponse => {
          this.hopitals = reponse;
     },error =>{console.log(error);}
      );
      }
    
    this.hopitalService.getHopitalByLocaliteAndSpecialisation(this.localite,this.specialisation).subscribe(reponse => {
      this.hopitals = reponse;
 },error =>{console.log(error);}
  );
   }

    public Clear(){
  this.hopitalService.gethopitals()
  .subscribe(reponse => {
       this.hopitals = reponse;
  },error =>{console.log(error);}
  );
   }

    public SearchByLocaliteAndSpecialisationId(localite:string,specialisationId:number){
  if(localite ==""){ this.hopitalService.getHopitalBySpecialisationId(this.specialisationId)
   .subscribe(reponse => {
     this.hopitals = reponse;
},error =>{console.log(error);}
 );
 }
 else if(specialisationId == null)
   { this.hopitalService.getHopitalByLocalite(this.localite)
     .subscribe(reponse => {
       this.hopitals = reponse;
  },error =>{console.log(error);}
   );
   }else
 {this.hopitalService.getHopitalByLocaliteAndSpecialisationId(this.localite,this.specialisationId).subscribe(reponse => {
   this.hopitals = reponse;
},error =>{console.log(error);}
);
   }
  }

   public DeleteHopital(id:number){
  this.hopitalService.deleteHopital(id).subscribe(reponse => {
    
   console.log(id);
},error =>{console.log(error);}
);
   }


}


