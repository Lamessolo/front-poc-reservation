import { HttpClient, HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule,routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { HopitalsComponent } from './hopitals/hopitals.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SpecialisationsComponent } from './specialisations/specialisations.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchFilterPipe } from './search-filter.pipe';
import { ReservationsComponent } from './reservations/reservations.component';
import { HopitalformComponent } from './hopitalform/hopitalform.component';
import { HopitalDetailsComponent } from './hopital-details/hopital-details.component';
import { MatDialogModule } from '@angular/material/dialog';
import { ReservationFormComponent } from './reservation-form/reservation-form.component';
import { ToastrModule } from 'ngx-toastr';
import { AccidentsComponent } from './accidents/accidents.component';
@NgModule({
  declarations: [
    AppComponent,
    HopitalsComponent,
    SpecialisationsComponent,
    NavBarComponent,
    SearchFilterPipe,
    ReservationsComponent,
    HopitalDetailsComponent,
    ReservationFormComponent,
    HopitalformComponent,
    routingComponents,
    AccidentsComponent
  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    FormsModule,
    ReactiveFormsModule,
    MatDialogModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(), // ToastrModule added
    
  ],
  providers: [],
  bootstrap: [AppComponent],
  entryComponents:[ReservationFormComponent,HopitalformComponent]
 
})
export class AppModule { }
