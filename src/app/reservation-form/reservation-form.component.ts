import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Ihopital } from '../Ihopital';
import { Ireservation } from '../Ireservation';
import { ReservationService } from '../reservation.service';
import { SpecialisationService } from '../specialisation.service';
import { specialisationInfo } from '../specialisationInfo';

@Component({
  selector: 'app-reservation-form',
  templateUrl: './reservation-form.component.html',
  styleUrls: ['./reservation-form.component.css']
})
export class ReservationFormComponent implements OnInit {

  public hopitals!:Ihopital[];
  hopitalReservation : Ireservation[];
  specialisations! : specialisationInfo[];
  msgValidReservation =false;
  reservationForm:FormGroup
  data: Ireservation[];
  constructor(private reservationService:ReservationService,  private fb : FormBuilder,
     private  specialisationService: SpecialisationService,private route:ActivatedRoute) { }

  ngOnInit(): void {
   
    this.reservationForm = this.fb.group({
  date:'', emetteurName:'',hopitalDestination:'',nbrLitReservation:'',details:'', status:'',hopitalId:''
     });
    this.reservationService.getReservation().subscribe(data =>{
       this.hopitalReservation = data;
    });
  }

  public onSubmitReservation(){
   
this.reservationService.createReservation(this.reservationForm.value).subscribe(data =>{
  console.log(data);
 this.msgValidReservation=true;
});

}

}
