import { Injectable } from '@angular/core';
import { Ihopital } from './Ihopital';
import { Observable, observable } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import { map } from 'rxjs/operators';
import { specialisationInfo } from './specialisationInfo';
import { FormGroup } from '@angular/forms';
import { environment } from 'src/environments/environment';


@Injectable({
  providedIn: 'root'
})
export class HopitalService {
  
  constructor(private httpClient: HttpClient) { }

  private baseUrlHopital = environment.apiUrl +'hopitals';  
public  gethopitals(): Observable<Ihopital[]>{
    
   return this.httpClient.get<Ihopital[]>(this.baseUrlHopital);
  }

public getHopitalByLocalite(localite:string): Observable<Ihopital[]>{
  return this.httpClient.get<Ihopital[]>(this.baseUrlHopital +'/localite/'+localite);
}


public  gethopitalById(id:number): Observable<Ihopital>{   
  return this.httpClient.get<Ihopital[]>(this.baseUrlHopital).pipe(
    map((hopitals:any[])=>hopitals.find(hopital=>hopital.id === id))
  );  
 }
 // Cherche les hopitaux en fonction de la specialisation indiqué 
 public getHopitalBySpecialisation(specialisation:string):Observable<Ihopital[]>{
    return this.httpClient.get<Ihopital[]>(this.baseUrlHopital+'/specialisationName/'+specialisation);

 }

 public getHopitalByLocaliteAndSpecialisation(localite:string,specialisation:string){
  return this.httpClient.get<Ihopital[]>(this.baseUrlHopital+'/'+ localite+'/'+ specialisation);
 }

public getHopitalByLocaliteAndSpecialisationId(localite:string,specialisation:number){
  return this.httpClient.get<Ihopital[]>(this.baseUrlHopital+'/localite/'+ localite+'/'+ specialisation);
 }

 // Cherche les hopitaux en fonction de l'id de la specialisation
 public getHopitalBySpecialisationId(id:number):Observable<Ihopital[]>{
  return this.httpClient.get<Ihopital[]>(this.baseUrlHopital+'/specialisationId/'+id);

}

public createHopital(newHopital:Ihopital){
  return this.httpClient.post(this.baseUrlHopital+'/new',newHopital);
}

public deleteHopital (id:number) {
    return this.httpClient.delete(this.baseUrlHopital+'/'+id);
}

}

