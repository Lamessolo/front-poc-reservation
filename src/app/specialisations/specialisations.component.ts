import { Component, OnInit } from '@angular/core';
import { SpecialisationService } from '../specialisation.service';
import { specialisationInfo } from '../specialisationInfo';

@Component({
  selector: 'app-specialisations',
  templateUrl: './specialisations.component.html',
  styleUrls: ['./specialisations.component.css']
})
export class SpecialisationsComponent implements OnInit { 
  specialisations!: specialisationInfo[];
  search!:string;
  id:number=1;
  constructor(private specialisationService : SpecialisationService) { }

  ngOnInit(): void {
    
    this.specialisationService.getspecialisation().subscribe(reponse => {
      this.specialisations = reponse;
      });
    }

    public Search(){     
      this.specialisationService.getspecialisationByNameOrSpecialite(this.search).subscribe(reponse => {
          this.specialisations= reponse;
     },error =>{console.log(error);}
      );   
    }

    public Clear(){
      this.specialisationService.getspecialisation().subscribe(reponse => {
        this.specialisations = reponse;
   });
    }

}
