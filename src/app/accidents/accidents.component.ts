import { Component, OnInit } from '@angular/core';
import { accident } from '../accident';
import { AccidentService } from '../accident.service';

@Component({
  selector: 'app-accidents',
  templateUrl: './accidents.component.html',
  styleUrls: ['./accidents.component.css']
})
export class AccidentsComponent implements OnInit {
  public accidents!: accident[];
  public accidentId : number;
 public urgent: string;
 public normal: string;
  constructor(private accidentService : AccidentService) { }

  ngOnInit(): void {

    this.getAccident();
  }

  public getAccident(){
    this.accidentService.getAccidents()
    .subscribe(reponse => {
         this.accidents = reponse;
    },error =>{console.log(error);}
    );
  }



}
